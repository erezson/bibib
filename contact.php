<?php 
/*
Template Name: Contact
*/ 
get_header();?>
<div class="content_with_padding">
  <div class="flex_section">

    <div class="about_pages_illustration">
      <div class="very_large_title left_spacing"><h1>Contact Us</h1></div>
    </div>
    
    <div class="about_pages_texts clearfix" >
    <?php echo do_shortcode( '[contact-form-7 id="141" title="Contact us"]' ); ?>
         </div>
  </div>
</div>

<?php get_footer(); // подключаем footer.php ?>