<?php
/**
 * Страница 404 ошибки (404.php)
 * @package WordPress
 * @subpackage your-clean-template
 */
get_header(); // Подключаем header.php ?>
<div class="center" style="margin-top: 15%; font-size: 120px;">
<h1>404!</h1>
<p>Nothing found</p>
<a href="/">Go Home</a>
</div>
<?php get_footer(); // подключаем footer.php ?>