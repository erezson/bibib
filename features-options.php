<?php

// Default options values
$sa_options1 = array(
	'small_text' => '',
	'small_text1' => '',
	'title_text' => '',
	'desciption_text1' => '',
	'desciption_text2' => '',
	'cta_text' => '',	
	'cta_text32' => '',
	'cta_text3' => '',
	'background2' => '',
	'background3' => '',
	'background41' => '',	
	'alts_textik' => '',
	'alts_text' => '',
	'alts_texts' => '',
	'alts_textss' => '',
	'alts_textsss' => '',
	'alts_textsss1' => '',
	'alts_textsss2' => '',
	'alts_textsss3' => '',
	'alts_textsss4' => '',
	'alts_textsss5' => '',
	'alts_textsss6' => '',
	'alts_textsss7' => '',
	'background4' => '',
	'copirate' => '',
	'contactus' => '',
	'contactus1' => '',
	'twit' => '',
	'folus' => '',
	'fas' => '',
	'write' => '',
	'afwrite' => '',
	'writeemail' => '',
	'week1' => '',
	'week2' => '',
	'week3' => '',
	'week4' => '',
);

if ( is_admin() ) : // Load only if we are viewing an admin page

function sa_register_settings1() {
	// Register settings and call sanitation functions
	register_setting( 'sa_theme_options1', 'sa_options1', 'sa_validate_options1' );
}

add_action( 'admin_init', 'sa_register_settings1' );


function sa_theme_options1() {
	// Add theme options page to the addmin menu
	add_menu_page( 'Features Options', 'Index Options', 'edit_theme_options', 'theme_options1', 'sa_theme_options_page1' );
}

add_action( 'admin_menu', 'sa_theme_options1' );

// Function to generate options page
function sa_theme_options_page1() {
	global $sa_options1;

	if ( ! isset( $_REQUEST['updated'] ) )
		$_REQUEST['updated'] = false; // This checks whether the form has just been submitted. ?>

	<div class="wrap">

	<?php screen_icon(); echo "<h2>Index page Options</h2>";
	// This shows the page's name and an icon if one has been provided ?>

	<?php if ( false !== $_REQUEST['updated'] ) : ?>
	<div class="updated fade"><p><strong><?php _e( 'Options saved' ); ?></strong></p></div>
	<?php endif; // If the form has just been submitted, this shows the notification ?>

	<form method="post" action="options.php">
	<?php wp_nonce_field('update-options'); ?>
	<?php $settings1 = get_option( 'sa_options1', $sa_options1 ); ?>
	
	<?php settings_fields( 'sa_theme_options1' );
	/* This function outputs some hidden fields required by the form,
	including a nonce, a unique number used to ensure the form has been submitted from the admin page
	and not somewhere else, very important for security */ ?>

	<table class="form-table customme"><!-- Grab a hot cup of coffee, yes we're using tables! -->
	<tr>
		<th scope="row">
			<label>Upload an image for logo</label>
		</th>
			<td>
				<input type="text" id="background2" name="sa_options1[background2]" value="<?php echo esc_url( $settings1['background2'] ); ?>" />
				<input id="my_file_upload" type="button"  class="button upload_image_button" value="<?php _e( 'Upload Image', 'wptuts' ); ?>" />
			</td>
	</tr>
	<tr>
		<th scope="row">
			<label>Preview of the logo</label>
		</th>
			<td>
				<img style="max-width: 300px;" id="previewBG" src="<?php echo esc_url( $settings1['background2'] ); ?>" alt="banner">
			</td>
	</tr>
	<tr valign="top">
		<th scope="row">
			<label>logo Title:</label>
		</th>
			<td>
				<input style="width: 550px" type="text" id="small_text" name="sa_options1[small_text]" value="<?php echo ($settings1['small_text']); ?>">
			</td>
	</tr>
	
	</table>

	<p class="submit"><input type="submit" class="button-primary" id="saveHero" value="Save Options" /></p>

	</form>

	</div>

	<?php
}

function sa_validate_options1( $input ) {
	global $sa_options1;

	$settings1 = get_option( 'sa_options1', $sa_options1 );
	
	// We strip all tags from the text field, to avoid vulnerablilties like XSS
	// $input['small_text'] = wp_filter_nohtml_kses( $input['small_text'] );
	// $input['title_text'] = wp_filter_nohtml_kses( $input['title_text'] );
	// $input['desciption_text'] = wp_filter_nohtml_kses( $input['desciption_text'] );
	// $input['cta_text'] = wp_filter_nohtml_kses( $input['cta_text'] );
	
	// We strip all tags from the text field, to avoid vulnerablilties like XSS
	// $input['small_text'] = wp_filter_post_kses( $input['small_text'] );
	// $input['title_text'] = wp_filter_post_kses( $input['title_text'] );
	// $input['desciption_text'] = wp_filter_post_kses( $input['desciption_text'] );
	// $input['cta_text'] = wp_filter_post_kses( $input['cta_text'] );

	
	return $input;
}
	function wptuts_options_enqueue_scripts() {
		wp_register_script( 'wptuts-upload', get_template_directory_uri() .'/js/uploader.js', array('jquery','media-upload','thickbox') );


			wp_enqueue_script('jquery');

			wp_enqueue_script('thickbox');
			wp_enqueue_style('thickbox');

			wp_enqueue_script('media-upload');
			wp_enqueue_script('wptuts-upload');



	}
	add_action('admin_enqueue_scripts', 'wptuts_options_enqueue_scripts');

endif;  // EndIf is_admin()