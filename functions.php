<?php
/**
 * Функции шаблона (function.php)
 * @package WordPress
 * @subpackage your-clean-template
 */

function typical_title() { // функция вывода тайтла
  global $page, $paged; // переменные пагинации должны быть глобыльными
  wp_title('|', true, 'right'); // вывод стандартного заголовка с разделителем "|"
  //bloginfo('name'); // вывод названия сайта
  $site_description = get_bloginfo('description', 'display'); // получаем описание сайта
  if ($site_description && (is_home() || is_front_page())) //если описание сайта есть и мы на главной
    echo " | $site_description"; // выводим описание сайта с "|" разделителем
  if ($paged >= 2 || $page >= 2) // если пагинация была использована
    echo ' | '.sprintf(__( 'Page %s'), max($paged, $page)); // покажем номер страницы с "|" разделителем
}

register_nav_menus(array( // Регистрируем 2 меню
  'top' => 'Top', // Верхнее
));

add_theme_support('post-thumbnails'); // включаем поддержку миниатюр
set_post_thumbnail_size(250, 150); // задаем размер миниатюрам 250x150
add_image_size('big-thumb', 400, 400, true); // добавляем еще один размер картинкам 400x400 с обрезкой

register_sidebar(array( // регистрируем левую колонку, этот кусок можно повторять для добавления новых областей для виджитов
  'name' => 'left Column', // Название в админке
  'id' => "left-sidebar", // идентификатор для вызова в шаблонах
  'description' => 'Regular column in the sidebar', // Описалово в админке
  'before_widget' => '<div id="%1$s" class="widget %2$s">', // разметка до вывода каждого виджета
  'after_widget' => "</div>\n", // разметка после вывода каждого виджета
  'before_title' => '<span class="widgettitle">', //  разметка до вывода заголовка виджета
  'after_title' => "</span>\n", //  разметка после вывода заголовка виджета
));



function true_loadmore_scripts() {
  if (is_home()) {
  wp_enqueue_script('jquery'); // скорее всего он уже будет подключен, это на всякий случай
   wp_enqueue_script( 'true_loadmore', get_stylesheet_directory_uri() . '/js/loadmore.js', array('jquery') );
  }
}
 
add_action( 'wp_enqueue_scripts', 'true_loadmore_scripts' );


function true_countdown(){  
  wp_enqueue_script('jquery');
  wp_enqueue_script( 'plugin-count-new', get_stylesheet_directory_uri() . '/js/plugin-count.min.js', array('jquery') );
  wp_enqueue_script( 'countdown-new', get_stylesheet_directory_uri() . '/js/countdown.min.js', array('jquery') );
  wp_enqueue_script( 'new-custom', get_stylesheet_directory_uri() . '/js/new-custom.js', array('jquery') );
}
add_action( 'wp_enqueue_scripts', 'true_countdown' );


function true_load_posts(){
  $args = unserialize(stripslashes($_POST['query']));
  //var_dump($args);
  $args['paged'] = $_POST['page'] + 1;
  $args['post_status'] = 'publish';
  /*$args['category__not_in'] = '-2';*/
  $q = new WP_Query($args);
  if( $q->have_posts() ):
    while($q->have_posts()): $q->the_post();
      ?>
      <div class="box">
         <div class="thumb">
          <?php if ( has_post_thumbnail() ): ?>
            <a class="thumb_overlay" href="<?php get_permalink(); ?>" title="<?php the_title(); // заголовок поста ?>">
              <?php the_post_thumbnail(array(170, 170), array( 'class' => 'thumb_image' )); ?>
            </a>
        <?php endif; ?>                   
               <a href="<?php the_permalink() ?>" rel="bookmark" class="thumb_overlay">
           <div class="card_overlay card_wrapper">
             <div class="card_overlay moving_part">
               <div class="game_card_yellow_text bibib_font" style="text-align: center; font-size: 17.88px; padding-top: 2.98px;">
                 <?php the_title(); // заголовок поста ?>
               </div>
             </div>
           </div>
         </a>
        </div>
  </div>
      <?php
    endwhile;
  endif;
  wp_reset_postdata();
  die();
}
 
 
add_action('wp_ajax_loadmore', 'true_load_posts');
add_action('wp_ajax_nopriv_loadmore', 'true_load_posts');


add_filter( 'body_class','my_body_classes' );
function my_body_classes( $classes ) {
 
    if ( is_single()) {
     
        $classes[] = 'new_game_page';
         
    }
     
    return $classes;
     
}

class Custom_Walker_Nav_Menu extends Walker_Nav_Menu {

  function start_lvl(&$output, $depth) {

      $indent = str_repeat("\t", $depth);
      //$output .= "\n$indent<ul class=\"sub-menu\">\n";
  global $sa_options1;
  $sa_settings1 = get_option( 'sa_options1', $sa_options1 );
$per = $sa_settings1['desciption_text1'];
$pers = $sa_settings1['desciption_text2'];
        // Change sub-menu to dropdown menu
      $output .= "\n$indent<ul class=\"subnav\"><li class=\"subnav-first\">
                <div class=\"section-title mod-nav\"><span>$per</span>$pers</div>
              </li>\n";
  }

  function start_el ( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
    // Most of this code is copied from original Walker_Nav_Menu
    global $wp_query, $wpdb;
    $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

    $class_names = $value = '';

    $classes = empty( $item->classes ) ? array() : (array) $item->classes;
    $classes[] = 'menu-item-' . $item->ID;

    $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
    $class_names = ' class="' . esc_attr( $class_names ) . '"';

    $id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
    $id = strlen( $id ) ? ' id="' . esc_attr( $id ) . '"' : '';

    $has_children = $wpdb->get_var("SELECT COUNT(meta_id)
                            FROM wp_postmeta
                            WHERE meta_key='_menu_item_menu_item_parent'
                            AND meta_value='".$item->ID."'");

    $output .= $indent . '<li' . $id . $value . $class_names .'>';

    $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
    $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
    $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
    $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';


    // Check if menu item is in main menu
    if ( $depth == 0 && $has_children > 0  ) {
        // These lines adds your custom class and attribute
        $attributes .= ' class="explode icon-angle-down js-subnav"';

    }

    $item_output = $args->before;
    $item_output .= '<a'. $attributes .'>';
    $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;   
    $item_output .= '</a>';
    $item_output .= $args->after;
 if ( $depth == 1  ) {
        // These lines adds your custom class and attribute
        $item_output = $args->before;
    $item_output .= '<a'. $attributes .'>';
        $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;   
      $item_output .= '</a>';
      $item_output .= $args->after;
          }
 

    $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
  }

}
require_once ( get_stylesheet_directory() . '/features-options.php' );


/* Header Widget */ 
function erez_widgets(){

	register_sidebar(array(
		'name' => esc_html__('Header Widget',''),
		'id' => 'header-widget',
		'description' => 'Located on header. You can add your custom social links. ',
		'before_widget' => '<div class="erez-widget widget-wrap">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widget-title" style="display:none">',
		'after_title' => '</h4>'
	));

}
add_action('widgets_init','erez_widgets');
