<?php
/**
 * Шаблон отдельной записи (single.php)
 * @package WordPress
 * @subpackage your-clean-template
 */
get_header(); // подключаем header.php ?>


<style>
    .top_block,
    .video_strip{
        position: relative;
    }
    .left_banner,
    .right_banner{
        display: block;
        position: absolute;
    }
    .left_banner {
        left: -175px;
        top: 0px;
    }
    .right_banner {
        right: -175px;
        top: 0px;
    }
    .top_banner {
        margin-top: 60px;
        margin-bottom: -75px;
    }
    .sticky-fb-chat-box{
        background-color: #3a5897;
        position: fixed;
        bottom: 0;
        right: 14px;
        width: 250px;
        height: 38px;
        z-index: 99;
        border-radius: 5px 5px 0 0;
    }
    .sticky-fb-chat-box img{
      display: inline-block;
      float: left;
      padding: 10px 20px;
    }
    .sticky-fb-chat-box .text{
      float: left;
      padding: 11px 10px;
    }
    .sticky-fb-chat-box .close{
      cursor: pointer;
      float: right;
      padding: 10px 14px;
      display: none!important;
    }
    #defaultCountdown{
      display: none!important;
    }

        

</style>


<!-- ## Start CDN Redirect -->

<?php
$cdn_mobile = get_field('cdn_mobile');
?>


<script>

  if(navigator.userAgent.match(/Android/i)

  || navigator.userAgent.match(/webOS/i)

  || navigator.userAgent.match(/iPhone/i)

  || navigator.userAgent.match(/iPad/i)

  || navigator.userAgent.match(/iPod/i)

  || navigator.userAgent.match(/BlackBerry/i)

  || navigator.userAgent.match(/Windows Phone/i)) { 

        var cdn_redirect = "<?php echo $cdn_mobile; ?>";

        if (cdn_redirect){
            window.location = cdn_redirect;
        } 

        $('.sticky-fb-chat-box').fadeOut();       

  }

//End CDN Redirect

/* Facebook Chat Bot */

  jQuery(function($){
    $('.sticky-fb-chat-box').find('.close').on('click',function(){
      $('.sticky-fb-chat-box').fadeOut();
    });
  });

/* End Facebook Chat Bot */


</script>


<?php 
  $fb_icon = get_template_directory_uri().'/img/facebook.png';
?>




<div id="defaultCountdown"></div>



<div id="content">
    <div class="top_banner">
        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
        <!-- Header BIBIB -->
        <ins class="adsbygoogle"
             style="display:inline-block;width:728px;height:90px"
             data-ad-client="ca-pub-1135102492221583"
             data-ad-slot="1136832758"></ins>
        <script>
            (adsbygoogle = window.adsbygoogle || []).push({});
        </script>
    </div>

    <?php if ( have_posts() ) while ( have_posts() ) : the_post(); // старт цикла ?>
<div id="game_data" class="game_data_container strips_bg">

      <div class="game_name_container">
<?php if ( has_post_thumbnail() ): ?>
  <?php the_post_thumbnail(array(90, 90), array( 'class' => 'game_thumb', 'alt' => the_title('','',false), 'title' => the_title('','',false) )); ?>
  <?php endif; ?>  
        <a href="/">
            <div title="BIBIB - Online Games World" class="bibib_games_icon"></div>
    </a>
        <h1 class="bibib_font"><div class="games_icon"></div><?php the_title(); // заголовок поста ?></h1>
      </div>

    </div>
    <div class="top_block">

        <div class="game_strip strips_bg clearfix">
            <div class="left_banner">
                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                <!-- Up left -->
                <ins class="adsbygoogle"
                     style="display:inline-block;width:160px;height:600px"
                     data-ad-client="ca-pub-1135102492221583"
                     data-ad-slot="7962095558"></ins>
                <script>
                    (adsbygoogle = window.adsbygoogle || []).push({});
                </script>
            </div>

          <div  class="fl similar_games_holder" id="left_similar">

    <div class="logotip">
              <a href="/">
                <div title="BIBIB - Online Games" class="bibib_games_iconss"><img src="<?php echo get_template_directory_uri(); // абсолютный путь до темы ?>/img/logos.png"></div>
              </a>
            </div>
    <?php $tags = wp_get_post_tags($post->ID);
    if ($tags) {
     $tag_ids = array();
     foreach($tags as $individual_tag) $tag_ids[] = $individual_tag->term_id;
     $args=array(
     'tag__in' => $tag_ids, // Сортировка производится по тегам
     'post__not_in' => array($post->ID),
     'showposts'=>2 // Цифра означает количество выводимых записей
     );
     $my_query = new wp_query($args);
     if( $my_query->have_posts() ) {

            while ($my_query->have_posts()) {
                $my_query->the_post();
            ?>

                <div class="similar_tile_wrapper"><div class="thumb">
                <a href="<?php the_permalink() ?>" rel="bookmark">
                  <?php if ( has_post_thumbnail() ): ?>
                    <?php the_post_thumbnail(array(110, 110), array( 'class' => 'thumb_image' )); ?>
                  <?php endif; ?>
                </a>
                        <a href="<?php the_permalink() ?>" rel="bookmark" class="thumb_overlay">
                  <div class="card_overlay card_wrapper">
                    <div class="card_overlay moving_part">
                      <div class="game_card_yellow_text bibib_font" >
                        <?php the_title(); // заголовок поста ?>
                      </div>
                    </div>
                  </div>
              </a>

                    </div></div>
            <?php
            }
        }
     wp_reset_query();
     $shove = 3;
     } else {
      $shove = 5;
     }
     ?>


    <?php
    $current_post_type = get_post_type( $post_id );
    $infocat = get_the_category();
     $postid = get_the_ID();
    $info = $infocat[0]->cat_ID;
    $args = array(
        'cat' => $info,
        'posts_per_page' => $shove,
        'orderby' => 'date',
        'post__not_in' => array( $postid )
    );
    $query = new WP_Query( $args );

    if ( $query->have_posts() ) {
        ?>
                <?php

                    while ( $query->have_posts() ) {

                        $query->the_post();

                        ?>
                            <div class="similar_tile_wrapper">
                              <div class="thumb ">
                              <a href="<?php the_permalink(); ?>">
                                <?php if ( has_post_thumbnail() ): ?>
                                  <?php the_post_thumbnail(array(110, 110), array( 'class' => 'thumb_image' )); ?>
                                  <?php endif; ?>
                                  </a>
                            <a href="<?php the_permalink() ?>" rel="bookmark" class="thumb_overlay">
                  <div class="card_overlay card_wrapper">
                    <div class="card_overlay moving_part">
                      <div class="game_card_yellow_text bibib_font" >
                        <?php the_title(); // заголовок поста ?>
                      </div>
                    </div>
                  </div>
              </a>

                               </div>
        </div>
                 <?php
                  }
                  ?>
        <?php
    }

    wp_reset_postdata();
    ?>
    </div>
          <div class="fl" id="game_box">
            <div id="PreRollAd" >
              <div id="PreRollDummy"><?php the_field('games'); ?></div>
            </div>
          </div>
            <div class="right_banner">
                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                <!-- Up right -->
                <ins class="adsbygoogle"
                     style="display:inline-block;width:160px;height:600px"
                     data-ad-client="ca-pub-1135102492221583"
                     data-ad-slot="1775961153"></ins>
                <script>
                    (adsbygoogle = window.adsbygoogle || []).push({});
                </script>
            </div>
    <br>
        </div>

    </div>


    <div class="game_data_container strips_bg" id="video_data">

            <div class="game_name_container">

                <div class="bibib_videos_icon"></div>

                <?php if ( has_post_thumbnail() ): ?>
                    <?php the_post_thumbnail(array(90, 90), array( 'class' => 'game_thumb', 'alt' => the_title('','',false), 'title' => the_title('','',false) )); ?>
                <?php endif; ?>
                <h1 class="bibib_font no_chapters"> <?php the_title(); ?></h1>
            </div>
            <div class="game_data_items bibib_font" id="chapters_links">
            </div>
        </div>
    <div class="top_block">

        <div id="video_on_gamepage" class="video_strip strips_bg" >
            <div class="left_banner">
<!--placement bottom-left -->
            </div>
            <div style="height:100%;" class="fl" id="video_left_vertical_similar"></div>

            <div id="video_box" class="fl" >
                <div id="VideoEmbed" >
                    <?php the_content(); // контент ?>
                </div>
                <!-- Adsense Start -->
                <br>
                <br>

<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- BIBIB 300 Bottom -->
<ins class="adsbygoogle"
     style="display:inline-block;width:300px;height:250px"
     data-ad-client="ca-pub-1135102492221583"
     data-ad-slot="9365092352"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
                <!-- Adsense Ends -->

            </div>

            <div class="right_banner">

<!-- Placement bottom right -->

            </div>
        </div>

    </div>
    </div>


  
</div>
<?php endwhile; // конец цикла ?>

<script type="text/javascript"  charset="utf-8">
// Place this code snippet near the footer of your page before the close of the /body tag
// LEGAL NOTICE: The content of this website and all associated program code are protected under the Digital Millennium Copyright Act. Intentionally circumventing this code may constitute a violation of the DMCA.
                            
eval(function(p,a,c,k,e,d){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--){d[e(c)]=k[c]||e(c)}k=[function(e){return d[e]}];e=function(){return'\\w+'};c=1};while(c--){if(k[c]){p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c])}}return p}(';q P=\'\',2a=\'2b\';1Q(q i=0;i<12;i++)P+=2a.11(C.K(C.O()*2a.G));q 2p=2,2B=4u,3b=4v,2T=4w,33=D(t){q o=!1,i=D(){z(k.1i){k.2E(\'2N\',e);F.2E(\'1V\',e)}S{k.2L(\'2F\',e);F.2L(\'2c\',e)}},e=D(){z(!o&&(k.1i||4x.2A===\'1V\'||k.2U===\'2V\')){o=!0;i();t()}};z(k.2U===\'2V\'){t()}S z(k.1i){k.1i(\'2N\',e);F.1i(\'1V\',e)}S{k.2G(\'2F\',e);F.2G(\'2c\',e);q n=!1;32{n=F.4t==4s&&k.1X}2X(d){};z(n&&n.2k){(D r(){z(o)H;32{n.2k(\'16\')}2X(e){H 4o(r,50)};o=!0;i();t()})()}}};F[\'\'+P+\'\']=(D(){q t={t$:\'2b+/=\',4p:D(e){q a=\'\',l,d,i,s,c,r,n,o=0;e=t.n$(e);1b(o<e.G){l=e.19(o++);d=e.19(o++);i=e.19(o++);s=l>>2;c=(l&3)<<4|d>>4;r=(d&15)<<2|i>>6;n=i&63;z(2M(d)){r=n=64}S z(2M(i)){n=64};a=a+10.t$.11(s)+10.t$.11(c)+10.t$.11(r)+10.t$.11(n)};H a},13:D(e){q n=\'\',l,c,a,s,d,i,r,o=0;e=e.1r(/[^A-4q-4r-9\\+\\/\\=]/g,\'\');1b(o<e.G){s=10.t$.1O(e.11(o++));d=10.t$.1O(e.11(o++));i=10.t$.1O(e.11(o++));r=10.t$.1O(e.11(o++));l=s<<2|d>>4;c=(d&15)<<4|i>>2;a=(i&3)<<6|r;n=n+U.V(l);z(i!=64){n=n+U.V(c)};z(r!=64){n=n+U.V(a)}};n=t.e$(n);H n},n$:D(t){t=t.1r(/;/g,\';\');q n=\'\';1Q(q o=0;o<t.G;o++){q e=t.19(o);z(e<1y){n+=U.V(e)}S z(e>4y&&e<4z){n+=U.V(e>>6|4G);n+=U.V(e&63|1y)}S{n+=U.V(e>>12|2C);n+=U.V(e>>6&63|1y);n+=U.V(e&63|1y)}};H n},e$:D(t){q o=\'\',e=0,n=4H=1z=0;1b(e<t.G){n=t.19(e);z(n<1y){o+=U.V(n);e++}S z(n>4I&&n<2C){1z=t.19(e+1);o+=U.V((n&31)<<6|1z&63);e+=2}S{1z=t.19(e+1);2I=t.19(e+2);o+=U.V((n&15)<<12|(1z&63)<<6|2I&63);e+=3}};H o}};q a=[\'4F==\',\'4E\',\'4A=\',\'4B\',\'4C\',\'4D=\',\'4n=\',\'4m=\',\'46\',\'47\',\'48=\',\'49=\',\'45\',\'44\',\'3Z=\',\'41\',\'42=\',\'43=\',\'4a=\',\'4b=\',\'4i=\',\'4j=\',\'4k==\',\'4l==\',\'4h==\',\'4g==\',\'4c=\',\'4d\',\'4e\',\'4f\',\'4J\',\'4K\',\'5f\',\'5g==\',\'3Y=\',\'5i=\',\'5e=\',\'5d==\',\'59=\',\'5a\',\'5b=\',\'5c=\',\'5j==\',\'5k=\',\'5r==\',\'5s==\',\'5t=\',\'5q=\',\'5p\',\'5l==\',\'5m==\',\'5n\',\'5o==\',\'58=\'],p=C.K(C.O()*a.G),w=t.13(a[p]),y=w,M=1,v=\'#57\',r=\'#4R\',g=\'#4S\',Y=\'#4T\',Q=\'\',W=\'4U 4Q!\',f=\'4P 4L 4M 4N\\\'4O 4V 4W 23 2n. 54\\\'s 55. 56 53\\\'t?\',b=\'52 4X 4Y 4Z\\\'t 51 5u 23 1W, 3z!\',s=\'I 3k, I 3h 3e 3v 23 2n.  3o 3p 3n!\',o=0,u=0,n=\'3c.3j\',l=0,A=e()+\'.2f\';D h(t){z(t)t=t.1R(t.G-15);q n=k.2h(\'3m\');1Q(q o=n.G;o--;){q e=U(n[o].1H);z(e)e=e.1R(e.G-15);z(e===t)H!0};H!1};D m(t){z(t)t=t.1R(t.G-15);q e=k.3q;x=0;1b(x<e.G){1g=e[x].1n;z(1g)1g=1g.1R(1g.G-15);z(1g===t)H!0;x++};H!1};D e(t){q o=\'\',e=\'2b\';t=t||30;1Q(q n=0;n<t;n++)o+=e.11(C.K(C.O()*e.G));H o};D i(o){q i=[\'3r\',\'3u==\',\'3l\',\'3i\',\'2o\',\'3d==\',\'3f=\',\'3g==\',\'3t=\',\'3X==\',\'3P==\',\'3O==\',\'3N\',\'3L\',\'3M\',\'2o\'],r=[\'2t=\',\'3w==\',\'3R==\',\'3V==\',\'3U=\',\'3T\',\'3K=\',\'3J=\',\'2t=\',\'3A\',\'3x==\',\'3y\',\'3C==\',\'3D==\',\'3I==\',\'3H=\'];x=0;1G=[];1b(x<o){c=i[C.K(C.O()*i.G)];d=r[C.K(C.O()*r.G)];c=t.13(c);d=t.13(d);q a=C.K(C.O()*2)+1;z(a==1){n=\'//\'+c+\'/\'+d}S{n=\'//\'+c+\'/\'+e(C.K(C.O()*20)+4)+\'.2f\'};1G[x]=21 27();1G[x].2e=D(){q t=1;1b(t<7){t++}};1G[x].1H=n;x++}};D L(t){};H{2j:D(t,d){z(3F k.N==\'3Q\'){H};q o=\'0.1\',d=y,e=k.1c(\'1q\');e.17=d;e.j.1h=\'1N\';e.j.16=\'-1m\';e.j.Z=\'-1m\';e.j.1e=\'29\';e.j.X=\'3E\';q a=k.N.37,r=C.K(a.G/2);z(r>15){q n=k.1c(\'28\');n.j.1h=\'1N\';n.j.1e=\'1A\';n.j.X=\'1A\';n.j.Z=\'-1m\';n.j.16=\'-1m\';k.N.3G(n,k.N.37[r]);n.1d(e);q i=k.1c(\'1q\');i.17=\'2W\';i.j.1h=\'1N\';i.j.16=\'-1m\';i.j.Z=\'-1m\';k.N.1d(i)}S{e.17=\'2W\';k.N.1d(e)};l=3B(D(){z(e){t((e.1T==0),o);t((e.1Y==0),o);t((e.1K==\'35\'),o);t((e.1F==\'2s\'),o);t((e.1M==0),o)}S{t(!0,o)}},2d)},1P:D(e,m){z((e)&&(o==0)){o=1;F[\'\'+P+\'\'].1B();F[\'\'+P+\'\'].1P=D(){H}}S{q b=t.13(\'3W\'),c=k.3s(b);z((c)&&(o==0)){z((2B%3)==0){q a=\'3S=\';a=t.13(a);z(h(a)){z(c.1I.1r(/\\s/g,\'\').G==0){o=1;F[\'\'+P+\'\'].1B()}}}};q v=!1;z(o==0){z((3b%3)==0){z(!F[\'\'+P+\'\'].2D){q l=[\'5h==\',\'5B==\',\'75=\',\'76=\',\'74=\'],s=l.G,d=l[C.K(C.O()*s)],n=d;1b(d==n){n=l[C.K(C.O()*s)]};d=t.13(d);n=t.13(n);i(C.K(C.O()*2)+1);q r=21 27(),u=21 27();r.2e=D(){i(C.K(C.O()*2)+1);u.1H=n;i(C.K(C.O()*2)+1)};u.2e=D(){o=1;i(C.K(C.O()*3)+1);F[\'\'+P+\'\'].1B()};r.1H=d;z((2T%3)==0){r.2c=D(){z((r.X<8)&&(r.X>0)){F[\'\'+P+\'\'].1B()}}};i(C.K(C.O()*3)+1);F[\'\'+P+\'\'].2D=!0};F[\'\'+P+\'\'].1P=D(){H}}}}},1B:D(){z(u==1){q M=3a.73(\'2Z\');z(M>0){H!0}S{3a.71(\'2Z\',(C.O()+1)*2d)}};q c=\'72==\';c=t.13(c);z(!m(c)){q h=k.1c(\'77\');h.1Z(\'78\',\'7d\');h.1Z(\'2A\',\'1l/7c\');h.1Z(\'1n\',c);k.2h(\'7b\')[0].1d(h)};79(l);k.N.1I=\'\';k.N.j.1a+=\'T:1A !14\';k.N.j.1a+=\'1D:1A !14\';q Q=k.1X.1Y||F.2m||k.N.1Y,p=F.7f||k.N.1T||k.1X.1T,d=k.1c(\'1q\'),y=e();d.17=y;d.j.1h=\'2y\';d.j.16=\'0\';d.j.Z=\'0\';d.j.X=Q+\'1x\';d.j.1e=p+\'1x\';d.j.2z=v;d.j.1U=\'70\';k.N.1d(d);q a=\'<a 1n="6Z://6P.6Q"><2r 17="2i" X="2x" 1e="40"><2v 17="2l" X="2x" 1e="40" 6O:1n="5v:2v/6N;6L,6M+6R+6S+B+B+B+B+B+B+B+B+B+B+B+B+B+B+B+B+B+B+B+B+B+B+B+B+B+B+B+B+B+B+B+B+6X+6Y+6W/6V/6T/6U/7e/7z+/7y/7A+7F/7D+7C/7w/7t/7k/7l/7u/7i+7g/7h+7m+7n+7q+7s+7j/7E+7v/7o+7p/7r+7B+7x+7a+6J/5U+5V/5T/5S/5P+5Q+5R/5W+5X+67+68+E+66/62/5Y/5Z/61/5O/+5N/6K++5C/5D/5A+5z/5w+5x+5y==">;</2r></a>\';a=a.1r(\'2i\',e());a=a.1r(\'2l\',e());q i=k.1c(\'1q\');i.1I=a;i.j.1h=\'1N\';i.j.1C=\'1L\';i.j.16=\'1L\';i.j.X=\'5E\';i.j.1e=\'5F\';i.j.1U=\'2q\';i.j.1M=\'.6\';i.j.2S=\'2R\';i.1i(\'5L\',D(){n=n.5M(\'\').5K().5J(\'\');F.38.1n=\'//\'+n});k.1E(y).1d(i);q o=k.1c(\'1q\'),R=e();o.17=R;o.j.1h=\'2y\';o.j.Z=p/7+\'1x\';o.j.5G=Q-5H+\'1x\';o.j.5I=p/3.5+\'1x\';o.j.2z=\'#69\';o.j.1U=\'2q\';o.j.1a+=\'J-1t: "6a 6y", 1u, 1v, 1w-1s !14\';o.j.1a+=\'6z-1e: 6x !14\';o.j.1a+=\'J-1k: 6w !14\';o.j.1a+=\'1l-1p: 1o !14\';o.j.1a+=\'1D: 6t !14\';o.j.1K+=\'1W\';o.j.36=\'1L\';o.j.6u=\'1L\';o.j.6v=\'2Q\';k.N.1d(o);o.j.6A=\'1A 6B 6H -6I 6G(0,0,0,0.3)\';o.j.1F=\'2Y\';q x=30,L=22,w=18,A=18;z((F.2m<2w)||(6F.X<2w)){o.j.34=\'50%\';o.j.1a+=\'J-1k: 6C !14\';o.j.36=\'6D;\';i.j.34=\'65%\';q x=22,L=18,w=12,A=12};o.1I=\'<39 j="1j:#6E;J-1k:\'+x+\'1J;1j:\'+r+\';J-1t:1u, 1v, 1w-1s;J-1S:6s;T-Z:1f;T-1C:1f;1l-1p:1o;">\'+W+\'</39><2H j="J-1k:\'+L+\'1J;J-1S:6r;J-1t:1u, 1v, 1w-1s;1j:\'+r+\';T-Z:1f;T-1C:1f;1l-1p:1o;">\'+f+\'</2H><6g j=" 1K: 1W;T-Z: 0.2J;T-1C: 0.2J;T-16: 24;T-2K: 24; 2P:6h 6f #6e; X: 25%;1l-1p:1o;"><p j="J-1t:1u, 1v, 1w-1s;J-1S:2O;J-1k:\'+w+\'1J;1j:\'+r+\';1l-1p:1o;">\'+b+\'</p><p j="T-Z:6b;"><28 6c="10.j.1M=.9;" 6d="10.j.1M=1;"  17="\'+e()+\'" j="2S:2R;J-1k:\'+A+\'1J;J-1t:1u, 1v, 1w-1s; J-1S:2O;2P-6i:2Q;1D:1f;6j-1j:\'+g+\';1j:\'+Y+\';1D-16:29;1D-2K:29;X:60%;T:24;T-Z:1f;T-1C:1f;" 6p="F.38.6q();">\'+s+\'</28></p>\'}}})();F.2g=D(t,e){q d=6o.6n,i=F.6k,r=d(),n,o=D(){d()-r<e?n||i(o):t()};i(o);H{6l:D(){n=1}}};q 2u;z(k.N){k.N.j.1F=\'2Y\'};33(D(){z(k.1E(\'26\')){k.1E(\'26\').j.1F=\'35\';k.1E(\'26\').j.1K=\'2s\'};2u=F.2g(D(){F[\'\'+P+\'\'].2j(F[\'\'+P+\'\'].1P,F[\'\'+P+\'\'].6m)},2p*2d)});',62,476,'|||||||||||||||||||style|document||||||var|||||||||if||vr6|Math|function||window|length|return||font|floor|||body|random|zRdbNLvXdGis|||else|margin|String|fromCharCode||width||top|this|charAt||decode|important||left|id||charCodeAt|cssText|while|createElement|appendChild|height|10px|thisurl|position|addEventListener|color|size|text|5000px|href|center|align|DIV|replace|serif|family|Helvetica|geneva|sans|px|128|c2|0px|mqdqsRbNPe|bottom|padding|getElementById|visibility|spimg|src|innerHTML|pt|display|30px|opacity|absolute|indexOf|KpmgdQqGwh|for|substr|weight|clientHeight|zIndex|load|block|documentElement|clientWidth|setAttribute||new||ad|auto||babasbmsgx|Image|div|60px|uYNJsMpbVA|ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789|onload|1000|onerror|jpg|gUYFQXvFYa|getElementsByTagName|FILLVECTID1|VUZnXIJQIL|doScroll|FILLVECTID2|innerWidth|blocker|cGFydG5lcmFkcy55c20ueWFob28uY29t|SIyWZQOaEE|10000|svg|none|ZmF2aWNvbi5pY28|jdYXnfFSfx|image|640|160|fixed|backgroundColor|type|fPGeuxbQca|224|ranAlready|removeEventListener|onreadystatechange|attachEvent|h1|c3|5em|right|detachEvent|isNaN|DOMContentLoaded|300|border|15px|pointer|cursor|leUlsSUbIC|readyState|complete|banner_ad|catch|visible|babn|||try|PCscJwTZcp|zoom|hidden|marginLeft|childNodes|location|h3|sessionStorage|QldPirUOja|moc|YS5saXZlc3BvcnRtZWRpYS5ldQ|disabled|YWdvZGEubmV0L2Jhbm5lcnM|YWR2ZXJ0aXNpbmcuYW9sLmNvbQ|have|YWQuZm94bmV0d29ya3MuY29t|kcolbdakcolb|understand|anVpY3lhZHMuY29t|script|in|Let|me|styleSheets|YWRuLmViYXkuY29t|querySelector|Y2FzLmNsaWNrYWJpbGl0eS5jb20|YWQubWFpbC5ydQ|my|YmFubmVyLmpwZw|c3F1YXJlLWFkLnBuZw|ZmF2aWNvbjEuaWNv|sorry|YWQtbGFyZ2UucG5n|setInterval|YmFubmVyX2FkLmdpZg|bGFyZ2VfYmFubmVyLmdpZg|468px|typeof|insertBefore|YWR2ZXJ0aXNlbWVudC0zNDMyMy5qcGc|d2lkZV9za3lzY3JhcGVyLmpwZw|Q0ROLTMzNC0xMDktMTM3eC1hZC1iYW5uZXI|YWRjbGllbnQtMDAyMTQ3LWhvc3QxLWJhbm5lci1hZC5qcGc|YWRzYXR0LmVzcG4uc3RhcndhdmUuY29t|YXMuaW5ib3guY29t|YWRzYXR0LmFiY25ld3Muc3RhcndhdmUuY29t|YWRzLnp5bmdhLmNvbQ|YWRzLnlhaG9vLmNvbQ|undefined|NDY4eDYwLmpwZw|Ly9wYWdlYWQyLmdvb2dsZXN5bmRpY2F0aW9uLmNvbS9wYWdlYWQvanMvYWRzYnlnb29nbGUuanM|MTM2N19hZC1jbGllbnRJRDI0NjQuanBn|c2t5c2NyYXBlci5qcGc|NzIweDkwLmpwZw|aW5zLmFkc2J5Z29vZ2xl|cHJvbW90ZS5wYWlyLmNvbQ|QWREaXY|QWQ3Mjh4OTA||QWRBcmVh|QWRGcmFtZTE|QWRGcmFtZTI|QWQzMDB4MjUw|QWQzMDB4MTQ1|YWQtZm9vdGVy|YWQtY29udGFpbmVy|YWQtY29udGFpbmVyLTE|YWQtY29udGFpbmVyLTI|QWRGcmFtZTM|QWRGcmFtZTQ|RGl2QWQ|RGl2QWQx|RGl2QWQy|RGl2QWQz|QWRzX2dvb2dsZV8wNA|QWRzX2dvb2dsZV8wMw|QWRMYXllcjE|QWRMYXllcjI|QWRzX2dvb2dsZV8wMQ|QWRzX2dvb2dsZV8wMg|YWQtbGI|YWQtbGFiZWw|setTimeout|encode|Za|z0|null|frameElement|196|211|193|event|127|2048|YWQtZnJhbWU|YWQtaGVhZGVy|YWQtaW1n|YWQtaW5uZXI|YWRCYW5uZXJXcmFw|YWQtbGVmdA|192|c1|191|RGl2QWRB|RGl2QWRC|looks|like|you|re|It|Gamer|526950|57b9ed|000000|Hi|using|an|our|games|don||work|But|doesn|That|okay|Who|001854|c3BvbnNvcmVkX2xpbms|YWRUZWFzZXI|YmFubmVyX2Fk|YWRCYW5uZXI|YWRiYW5uZXI|Z2xpbmtzd3JhcHBlcg|QWRDb250YWluZXI|RGl2QWRD|QWRJbWFnZQ|Ly93d3cuZ29vZ2xlLmNvbS9hZHNlbnNlL3N0YXJ0L2ltYWdlcy9mYXZpY29uLmljbw|QWRCb3gxNjA|YWRBZA|YmFubmVyYWQ|cG9wdXBhZA|YWRzZW5zZQ|Z29vZ2xlX2Fk|b3V0YnJhaW4tcGFpZA|YWRzbG90|YmFubmVyaWQ|IGFkX2JveA|YWRfY2hhbm5lbA|YWRzZXJ2ZXI|with|data|Uv0LfPzlsBELZ|3eUeuATRaNMs0zfml|gkJocgFtzfMzwAAAABJRU5ErkJggg|dEflqX6gzC4hd1jSgz0ujmPkygDjvNYDsU0ZggjKBqLPrQLfDUQIzxMBtSOucRwLzrdQ2DFO0NDdnsYq0yoJyEB0FHTBHefyxcyUy8jflH7sHszSfgath4hYwcD3M29I5DMzdBNO2IFcC5y6HSduof4G5dQNMWd4cDcjNNeNGmb02|uJylU|Ly93d3cuZ3N0YXRpYy5jb20vYWR4L2RvdWJsZWNsaWNrLmljbw|u3T9AbDjXwIMXfxmsarwK9wUBB5Kj8y2dCw|Kq8b7m0RpwasnR|160px|40px|minWidth|120|minHeight|join|reverse|click|split|QhZLYLN54|14XO7cR5WV1QBedt3c|x0z6tauQYvPxwT0VM1lH9Adt5Lp|F2Q|bTplhb|pyQLiBu8WDYgxEZMbeEqIiSM8r|kmLbKmsE|uI70wOsgFWUQCfZC1UI0Ettoh66D|szSdAtKtwkRRNnCIiDzNzc0RO|E5HlQS6SHvVSU0V|j9xJVBEEbWEXFVZQNX9|CGf7SAP2V6AjTOUa8IzD3ckqe2ENGulWGfx9VKIBB72JM1lAuLKB3taONCBn3PY0II5cFrLr7cCp|UIWrdVPEp7zHy7oWXiUgmR3kdujbZI73kghTaoaEKMOh8up2M8BVceotd||BNyENiFGe5CxgZyIT6KVyGO2s5J5ce|SRWhNsmOazvKzQYcE0hV5nDkuQQKfUgm4HmqA2yuPxfMU1m4zLRTMAqLhN6BHCeEXMDo2NsY8MdCeBB6JydMlps3uGxZefy7EO1vyPvhOxL7TPWjVUVvZkNJ||||MjA3XJUKy|1HX6ghkAR9E5crTgM|0t6qjIlZbzSpemi|fff|Arial|35px|onmouseover|onmouseout|CCC|solid|hr|1px|radius|background|requestAnimationFrame|clear|fYOjgIFaRc|now|Date|onclick|reload|500|200|12px|marginRight|borderRadius|16pt|normal|Black|line|boxShadow|14px|18pt|45px|999|screen|rgba|24px|8px|UADVgvxHBzP9LUufqQDtV|e8xr8n5lpXyn|base64|iVBORw0KGgoAAAANSUhEUgAAAKAAAAAoCAMAAABO8gGqAAAB|png|xlink|blockadblock|com|1BMVEXr6|sAAADr6|v792dnbbdHTZYWHZXl7YWlpZWVnVRkYnJib8|PzNzc3myMjlurrjsLDhoaHdf3|Ly8vKysrDw8O4uLjkt7fhnJzgl5d7e3tkZGTYVlZPT08vLi7OCwu|fn5EREQ9PT3SKSnV1dXks7OsrKypqambmpqRkZFdXV1RUVHRISHQHR309PTq4eHp3NzPz8|sAAADMAAAsKysKCgokJCRycnIEBATq6uoUFBTMzMzr6urjqqoSEhIGBgaxsbHcd3dYWFg0NDTmw8PZY2M5OTkfHx|enp7TNTUoJyfm5ualpaV5eXkODg7k5OTaamoqKSnc3NzZ2dmHh4dra2tHR0fVQUFAQEDPExPNBQXo6Ohvb28ICAjp19fS0tLnzc29vb25ubm1tbWWlpaNjY3dfX1oaGhUVFRMTEwaGhoXFxfq5ubh4eHe3t7Hx8fgk5PfjY3eg4OBgYF|http|9999|setItem|Ly95dWkueWFob29hcGlzLmNvbS8zLjE4LjEvYnVpbGQvY3NzcmVzZXQvY3NzcmVzZXQtbWluLmNzcw|getItem|Ly93d3cuZG91YmxlY2xpY2tieWdvb2dsZS5jb20vZmF2aWNvbi5pY28|Ly9hZHZlcnRpc2luZy55YWhvby5jb20vZmF2aWNvbi5pY28|Ly9hZHMudHdpdHRlci5jb20vZmF2aWNvbi5pY28|link|rel|clearInterval|UimAyng9UePurpvM8WmAdsvi6gNwBMhPrPqemoXywZs8qL9JZybhqF6LZBZJNANmYsOSaBTkSqcpnCFEkntYjtREFlATEtgxdDQlffhS3ddDAzfbbHYPUDGJpGT|head|css|stylesheet|aa2thYWHXUFDUPDzUOTno0dHipqbceHjaZ2dCQkLSLy|innerHeight|EuJ0GtLUjVftvwEYqmaR66JX9Apap6cCyKhiV|RUIrwGk|0idvgbrDeBhcK|uWD20LsNIDdQut4LXA|VOPel7RIdeIBkdo|HY9WAzpZLSSCNQrZbGO1n4V4h9uDP7RTiIIyaFQoirfxCftiht4sK8KeKqPh34D2S7TsROHRiyMrAxrtNms9H5Qaw9ObU1H4Wdv8z0J8obvOo|qdWy60K14k|CXRTTQawVogbKeDEs2hs4MtJcNVTY2KgclwH2vYODFTa4FQ|BKpxaqlAOvCqBjzTFAp2NFudJ5paelS5TbwtBlAvNgEdeEGI6O6JUt42NhuvzZvjXTHxwiaBXUIMnAKa5Pq9SL3gn1KAOEkgHVWBIMU14DBF2OH3KOfQpG2oSQpKYAEdK0MGcDg1xbdOWy|iqKjoRAEDlZ4soLhxSgcy6ghgOy7EeC2PI4DHb7pO7mRwTByv5hGxF|1FMzZIGQR3HWJ4F1TqWtOaADq0Z9itVZrg1S6JLi7B1MAtUCX1xNB0Y0oL9hpK4|I1TpO7CnBZO|YbUMNVjqGySwrRUGsLu6|Lnx0tILMKp3uvxI61iYH33Qq3M24k|wd4KAnkmbaePspA|0nga14QJ3GOWqDmOwJgRoSme8OOhAQqiUhPMbUGksCj5Lta4CbeFhX9NN0Tpny|oGKmW8DAFeDOxfOJM4DcnTYrtT7dhZltTW7OXHB1ClEWkPO0JmgEM1pebs5CcA2UCTS6QyHMaEtyc3LAlWcDjZReyLpKZS9uT02086vu0tJa|h0GsOCs9UwP2xo6|b29vlvb2xn5|v7|ejIzabW26SkqgMDA7HByRAADoM7kjAAAAInRSTlM6ACT4xhkPtY5iNiAI9PLv6drSpqGYclpM5bengkQ8NDAnsGiGMwAABetJREFUWMPN2GdTE1EYhmFQ7L339rwngV2IiRJNIGAg1SQkFAHpgnQpKnZBAXvvvXf9mb5nsxuTqDN|QcWrURHJSLrbBNAxZTHbgSCsHXJkmBxisMvErFVcgE|MgzNFaCVyHVIONbx1EDrtCzt6zMEGzFzFwFZJ19jpJy2qx5BcmyBM|ISwIz5vfQyDF3X|KmSx|cIa9Z8IkGYa9OGXPJDm5RnMX5pim7YtTLB24btUKmKnZeWsWpgHnzIP5UucvNoDrl8GUrVyUBM4xqQ'.split('|'),0,{}));
</script>

<!-- Go to www.addthis.com/dashboard to customize your tools --> <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-58ac558aa7df1952"></script> 

<?php get_footer(); // подключаем footer.php ?>
