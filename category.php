<?php
/**
 * Шаблон рубрики (category.php)
 * @package WordPress
 * @subpackage your-clean-template
 */
get_header(); // подключаем header.php ?> 
<script type="text/javascript">
  jQuery(document).ready(function($) {
  var browserWindow = $(window);
var width = browserWindow.width();
var height = browserWindow.height();
  console.log(width);
});

</script>

<div id="thumbs_place_holder">
<div id="scrolling_games_div" class="selected_category_div_wrapper">
	<?php if (have_posts()) : while (have_posts()) : the_post(); // если посты есть - запускаем цикл wp ?>
		<div class="box">
   <div class="thumb">
  <?php if ( has_post_thumbnail( $_post->ID ) ) {
        echo '<a class="thumb_overlay" href="' . get_permalink( $_post->ID ) . '" title="' . esc_attr( $_post->post_title ) . '">';
        echo get_the_post_thumbnail( $_post->ID, 'thumbnail thumb_image' );
        echo '</a>';
    } ?>
          <a href="<?php the_permalink() ?>" rel="bookmark" class="thumb_overlay">
              <div class="card_overlay card_wrapper">
                <div class="card_overlay moving_part">
                  <div class="game_card_yellow_text kizi_font" style="text-align: center; font-size: 17.88px; padding-top: 2.98px;">
                    <?php the_title(); // заголовок поста ?>
                  </div>
                </div>
              </div>
          </a>
  </div>
  </div>
	<?php endwhile; // конец цикла
	else: echo '<h2>Nothing found.</h2>'; endif; // если записей нет, напишим "простите" ?>	 
</div>



<div id="scrolling_games_div">

<?php 
// The Query
$category = get_the_category();
$current_category = $category[0];

query_posts( 'cat=-"'.$current_category->cat_ID.'"' );
// The Loop
while ( have_posts() ) : the_post();
?>
  <div class="box">
  <div class="thumb">
  <?php if ( has_post_thumbnail( $_post->ID ) ) {
        echo '<a class="thumb_overlay" href="' . get_permalink( $_post->ID ) . '" title="' . esc_attr( $_post->post_title ) . '">';
        echo get_the_post_thumbnail( $_post->ID, 'thumbnail thumb_image' );
        echo '</a>';
    } ?>
      <a href="<?php the_permalink() ?>" rel="bookmark" class="thumb_overlay">
              <div class="card_overlay card_wrapper">
                <div class="card_overlay moving_part">
                  <div class="game_card_yellow_text kizi_font" style="text-align: center; font-size: 17.88px; padding-top: 2.98px;">
                    <?php the_title(); // заголовок поста ?>
                  </div>
                </div>
              </div>
          </a>
  </div>
  </div>
<?php
endwhile;
 
// Reset Query
wp_reset_query();
?>
</div>
<?php if (  $wp_query->max_num_pages > 1 ) : ?>
  <script id="true_loadmore">
  var ajaxurl = '<?php echo site_url() ?>/wp-admin/admin-ajax.php';
  var true_posts = '<?php echo serialize($wp_query->query_vars); ?>';
  var current_page = <?php echo (get_query_var('paged')) ? get_query_var('paged') : 1; ?>;
  </script>
<?php endif; ?>
	<!--<?php if (have_posts()) : while (have_posts()) : the_post(); // если посты есть - запускаем цикл wp ?>
		<?php get_template_part('loop'); // для отображения каждой записи берем шаблон loop.php ?>
	<?php endwhile; // конец цикла
	else: echo '<h2>Nothing found.</h2>'; endif; // если записей нет, напишим "простите" ?>	 -->

</div>
</div>
</div>
<?php get_footer(); // подключаем footer.php ?>
