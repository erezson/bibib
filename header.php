<?php
/**
 * Шаблон шапки (header.php)
 * @package WordPress
 * @subpackage your-clean-template
 */
global $sa_options1,$sa_options3;
$sa_settings1 = get_option( 'sa_options1', $sa_options1 );
$sa_settings3 = get_option( 'sa_options3', $sa_options3 );

?>
<!DOCTYPE html>
<html <?php language_attributes(); // вывод атрибутов языка ?>>
<head>
  <meta charset="<?php bloginfo( 'charset' ); // кодировка ?>">
  <?php /* RSS и всякое */ ?>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="alternate" type="application/rdf+xml" title="RDF mapping" href="<?php bloginfo('rdf_url'); ?>">
  <link rel="alternate" type="application/rss+xml" title="RSS" href="<?php bloginfo('rss_url'); ?>">
  <link rel="alternate" type="application/rss+xml" title="Comments RSS" href="<?php bloginfo('comments_rss2_url'); ?>">
  <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
  <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); // абсолютный путь до темы ?>/favorit.css">
  <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); // абсолютный путь до темы ?>/style1.css">
  <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); // абсолютный путь до темы ?>/style.css">
   <!--[if lt IE 9]>
   <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
   <![endif]-->
  <title>
  <?php   
  if (is_archive()) { 
  
    wp_title(''); echo ' | ';
    bloginfo('name'); 
  
  } elseif (is_search()) { 
  
    echo 'Search for &quot;'.wp_specialchars($s).'&quot; | '; 
    bloginfo('name');
  
  } elseif (!(is_404()) && (is_single()) || (is_page())) { 
  
    wp_title(''); echo ' | '; 
    bloginfo('name');
  
  } elseif (is_404()) {
  
    echo 'Not Found | '; 
    bloginfo('name');
  
  }
  elseif (is_home()) {
    bloginfo('name');
    wp_title(''); echo ' | ';
    bloginfo('description'); 
     
  }
  ?>
  </title>
  <?php wp_head(); // необходимо для работы плагинов и функционала ?>

<script type="text/javascript">

function progress(percent, $element) {
    var progressBarWidth = percent * $element.width() / 100;
    $element.find('div.infobar-overlayer').animate({ width: progressBarWidth }, 20000, function(){
      jQuery(".info-bar").fadeOut();
    });
}

jQuery(document).ready(function(){

  var divs = jQuery("span.info-rand").get().sort(function(){ 
              return Math.round(Math.random())-0.5; //so we get the right +/- combo
             }).slice(0,1);
  jQuery(divs).show();;

  function openInfoBar(){
    jQuery(".info-bar").fadeIn();
    progress(100, jQuery('.info-bar'));
  }
  setTimeout(openInfoBar, 60000);

  jQuery(".infobar-close").click(function(){
    jQuery(".info-bar").fadeOut();
  });

});


</script>

  <script type="text/javascript">
function loadArticle(pageNumber) {
    jQuery.ajax({
        url: "<?php bloginfo('wpurl') ?>/wp-admin/admin-ajax.php",
        type:'POST',
        data: "action=infinite_scroll&page_no="+ pageNumber + '&loop_file=loop', 
        success: function(html){
            jQuery("#main .container").append(html);    // This will be the div where our content will be loaded
        }
    });
    return false;
}
</script>


<script type="text/javascript">
        var count = 2;//for first load
        jQuery(window).scroll(function(){
                if  (jQuery(window).scrollTop() == jQuery(document).height() - jQuery(window).height()){
                   loadArticle(count);
                   count++;
                }
        }); 
 
</script>
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

// Creates an adblock detection plugin.
ga('provide', 'adblockTracker', function(tracker, opts) {
  var ad = document.createElement('ins');
  ad.className = 'AdSense';
  ad.style.display = 'block';
  ad.style.position = 'absolute';
  ad.style.top = '-1px';
  ad.style.height = '1px';
  document.body.appendChild(ad);
  tracker.set('dimension' + opts.dimensionIndex, !ad.clientHeight);
  document.body.removeChild(ad);
});

ga('create', 'UA-40627535-2', 'auto');
ga('require', 'adblockTracker', {dimensionIndex: 1});
ga('send', 'pageview');
</script>
<script src="https://apis.google.com/js/platform.js" async defer></script>

<style>

/* Header Social Links */
#header_container .erez-widget{
  display:inline-block;
}
#header_container .erez-widget img{
  max-width: 28px;
  height: auto;
}
#header_container .erez-widget-wrap{
	display:inline-block;
	float:left;
	margin-top: 10px;
}
#header_container .erez-widget-wrap .erez-widget{
	margin-right:9px;
}
@media(max-width:767px){
	#header_container .erez-widget{
		display:none!important;
	}
}

</style>

</head>
<body <?php body_class(); // все классы для body ?>>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId=518755621662319";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div id="content">

  <div id="header_container">
  <a href="<?php echo home_url(); ?>">
  <img alt="<?php echo $sa_settings1['small_text']; ?>" id="bibib_logo" src="<?php echo $sa_settings1['background2']; ?>" title="BIBIB">
  </a>
  <div class="categori">
<script type="text/javascript">

  jQuery(document).ready(function($) {
$(".nav-content").hide();
$(".button").click(function() {
    $(".nav-content:visible").slideUp("normal");
    if (($(this).next().is("ul")) && (!$(this).next().is(":visible"))) {
        $(this).next().slideDown("normal");
    }
});
});
</script>


 <!-- Begin Navigation site -->
      <?php $args = array( // arguments to display top menu, menu must be created in admin panel for arguments working
        'theme_location' => 'top', // menu identificator, defined of register_nav_menus() function in function.php
        'container'=> 'li', // parent tag of ul, false is nothing
        'container_class'=>'navigation-site col-md-9 col-sm-9 col-xs-3',
        'menu_class' => 'nav-content', // class of ul
        'items_wrap'  => '<button class="button">Categories <i>&#9660;</i></button><ul class="nav-content">%3$s</ul>',
        'walker' => new Custom_Walker_Nav_Menu,
        );
        wp_nav_menu($args); // display top menu
      ?>
      <!-- End Navigation site -->

  </div>
  <div class="right">
  <?php if ( is_active_sidebar( 'left-sidebar' ) ) { ?>
    <?php //dynamic_sidebar( 'left-sidebar' ); ?>
  <?php } ?>
  <div class="erez-widget-wrap">
 	<?php dynamic_sidebar('header-widget'); ?>
</div>
  <div class="cont">
    <a href="/contact" class="footer_nav_button" id="contact_button" title="Contact Us"><img alt="Small_envlope" src="<?php echo get_template_directory_uri(); // абсолютный путь до темы ?>/img/small_envlope.png" style="margin-top:6px;"></a>
  </div>

  <div class="fr" id="search_div">
 <?php echo do_shortcode('[wpdreams_ajaxsearchlite]'); ?>
    </div>
    </div>
    </div>
