<?php
/**
 * Главная страница (index.php)
 * @package WordPress
 * @subpackage your-clean-template
 */
get_header(); // подключаем header.php ?>


<div id="thumbs_place_holder" style="width: 100%;">
<div id="scrolling_games_div">

<?php echo do_shortcode('[dd_lastviewed widget_id="2"]'); ?> 


<?php $posts = get_posts ("category=2&orderby=date&numberposts=16"); ?> 
<?php if ($posts) : ?>
<?php foreach ($posts as $post) : setup_postdata ($post); ?>
 
  <div class="box">
   <div class="thumb">
  <?php if ( has_post_thumbnail( $_post->ID ) ) {
        echo '<a class="thumb_overlay" href="' . get_permalink( $_post->ID ) . '" title="' . esc_attr( $_post->post_title ) . '">';
        echo get_the_post_thumbnail( $_post->ID, 'thumbnail thumb_image' );
        echo '</a>';
    } ?>
          <a href="<?php the_permalink() ?>" rel="bookmark" class="thumb_overlay">
              <div class="card_overlay card_wrapper">
                <div class="card_overlay moving_part">
                  <div class="game_card_yellow_text bibib_font">
                    <?php the_title(); // заголовок поста ?>
                  </div>
                </div>
              </div>
          </a>
  </div>
  </div>
 
<?php 
  endforeach;
  wp_reset_postdata();
?>
<?php endif; ?>

<?php 
  
// Reset Query
wp_reset_query();
// The Query
query_posts( 'cat=17' );
 
// The Loop
while ( have_posts() ) : the_post();
?>
 <?php  ?>
  <div class="box">
  <div class="thumb">
  <?php if ( has_post_thumbnail( $_post->ID ) ) {
        echo '<a class="thumb_overlay" href="' . get_permalink( $_post->ID ) . '" title="' . esc_attr( $_post->post_title ) . '">';
        echo get_the_post_thumbnail( $_post->ID, 'thumbnail thumb_image' );
        echo '</a>';
    } ?>
      <a href="<?php the_permalink() ?>" rel="bookmark" class="thumb_overlay">
              <div class="card_overlay card_wrapper">
                <div class="card_overlay moving_part">
                  <div class="game_card_yellow_text bibib_font" >
                    <?php the_title(); // заголовок поста ?>
                  </div>
                </div>
              </div>
          </a>
  </div>
  </div>
<?php
endwhile;
 
?>
<?php if (  $wp_query->max_num_pages > 1 ) : ?>
  <script id="true_loadmore">
  var ajaxurl = '<?php echo site_url() ?>/wp-admin/admin-ajax.php';
  var true_posts = '<?php echo serialize($wp_query->query_vars); ?>';
  var current_page = <?php echo (get_query_var('paged')) ? get_query_var('paged') : 1; ?>;
  </script>
<?php endif;
 // Reset Query
wp_reset_query();
  ?>
  <!--<?php if (have_posts()) : while (have_posts()) : the_post(); // если посты есть - запускаем цикл wp ?>
    <?php get_template_part('loop'); // для отображения каждой записи берем шаблон loop.php ?>
  <?php endwhile; // конец цикла
  else: echo '<h2>Нет записей.</h2>'; endif; // если записей нет, напишим "простите" ?>   -->

</div>
</div>
<?php get_footer(); // подключаем footer.php ?>