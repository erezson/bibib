/******/ (function(modules) { // webpackBootstrap
/******/   // The module cache
/******/   var installedModules = {};

/******/   // The require function
/******/   function __webpack_require__(moduleId) {

/******/     // Check if module is in cache
/******/     if(installedModules[moduleId])
/******/       return installedModules[moduleId].exports;

/******/     // Create a new module (and put it into the cache)
/******/     var module = installedModules[moduleId] = {
/******/       exports: {},
/******/       id: moduleId,
/******/       loaded: false
/******/     };

/******/     // Execute the module function
/******/     modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/     // Flag the module as loaded
/******/     module.loaded = true;

/******/     // Return the exports of the module
/******/     return module.exports;
/******/   }


/******/   // expose the modules object (__webpack_modules__)
/******/   __webpack_require__.m = modules;

/******/   // expose the module cache
/******/   __webpack_require__.c = installedModules;

/******/   // __webpack_public_path__
/******/   __webpack_require__.p = "";

/******/   // Load entry module and return exports
/******/   return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports) {

  "use strict";

  var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  document.addEventListener("DOMContentLoaded", function () {

      (function (jQuery) {

          ///<summary>
          ///private function to check if jQuery is enabled
          ///retuns bool
          ///</summary>
          function isjQEnabled() {
              return "$" in window;
          }

          ///<summary>
          ///private function to click on add to favorite button
          ///</summary>
          function clickHandler() {
              var btn = jQuery('#add_to_favorite');
              if (navigator.userAgent.indexOf("Firefox") > -1) {
                  btn.click(function (e) {
                      var bookmarkURL = window.location.href + "?bookmark";
                      var bookmarkTitle = document.title;

                      if ('addToHomescreen' in window && addToHomescreen.isCompatible) {
                          // Mobile browsers
                          addToHomescreen({ autostart: false, startDelay: 0 }).show(true);
                      } else if (window.sidebar && window.sidebar.addPanel) {
                          // Firefox <=22
                          window.sidebar.addPanel(bookmarkTitle, bookmarkURL, '');
                      } else if (window.sidebar && /Firefox/i.test(navigator.userAgent) || window.opera && window.print) {
                          // Firefox 23+ and Opera <=14
                          jQuery(this).attr({
                              href: bookmarkURL,
                              title: bookmarkTitle,
                              rel: 'sidebar'
                          }).off(e);
                          return true;
                      } else if (window.external && 'AddFavorite' in window.external) {
                          // IE Favorites
                          try {
                              window.external.AddFavorite(bookmarkURL, bookmarkTitle);
                          } catch (err) {
                              alert('Press ' + (/Mac/i.test(navigator.userAgent) ? 'Cmd' : 'Ctrl') + '+D to bookmark this page.');
                          }
                      } else {
                          // Other browsers (mainly WebKit & Blink - Safari, Chrome, Opera 15+)
                          alert('Press ' + (/Mac/i.test(navigator.userAgent) ? 'Cmd' : 'Ctrl') + '+D to bookmark this page.');
                      }
                      return false;
                  });
              } else {
                  btn.click(function (e) {
                      e.preventDefault();
                  });
              }
          }

          ///<summary>
          ///private function to create html for plugin
          ///</summary>
          function createHtmlTemplate(linkImg, text) {
              var container = void 0,
                  textBlock = void 0,
                  closeBtn = void 0,
                  link = void 0,
                  img = void 0;
              var src = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAKcAAACnCAYAAAB0FkzsAAAACXBIWXMAAC4jAAAuIwF4pT92AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAACFhJREFUeNrsnd1120YQRmGdPBvsgEwFViogXIGpCkR1ID/5MfBjnkJVYKqCKB1QHUgdUB1IaUDZsQc6EEVJxM9iZ7H3OweBE4sUs7z8ZnY4A3x4fHzMUDv99/DX0p0W7viy81d37ti4Y/Ux/3bDSrXTB+BsBeWxO63d8emAH79wR+kgvWflgNM3mIU7Xbkjb/CwW3cUANpMRyxBY8dsCmamDrthBYHTp9YtwHwC1MFdsoSEdR+uKRuffzo+zYM7ZoR3nLNvnffwHLnu7hFw9uaaM3ea9/R0hHbg7FV9AjXVHT8Czs6uOXGnU4MpAnAiLyB90VQBAWcnLSNIFYAzwZAuYE49Pf1CUwYEnKZcU5R7fn7gHLFryo567vnXsDECTnOuWWmq3zwh4DzYNWUnfTrQr8M9gdOca1aaa7cTAk6TboZ7AudBIX2ZtW+La6tTykrAadnFcE/gfNM1i+ywuaDY81zgxDUbaaopBQLOF645y16O+OKewJm8a1aa0+sJnLuuOTHkWrgncD6TfIWYG3ktp/R6AmddpbHXg3sC59PI79TYy6LmCZxmQcgpKyUOZ88jv2NPNYATAJ6U/AhxsnBq+ch6o+85cKaba+bGX2PSI8Qpw7mM6EMEnAmFdAFzGsuHKNVez1SdcxnRa012hDg5OHVeZx7Zyz4HznQ2QrEpyRHipOD0dMU4PlTAmfwbPE+trASccakEznGG9GVmv+j+npK6Ml1KzjmGnC1PKfdMAs7AI799awmc49KY3tBkRohHD+fAV4zjwwacyeWau0riynSjhtPYyC8fOuB8Jksjv31r9CPEUd+YtTbGUJ0l1E30nGfpSO7nLjd73e4c9x/zbzfA6WcjM6vBVp1/5lwZaiK5W/GNAlzBupF/OHg3wPkSvuM94E0UyCk8BXPfm10XdgBvRwOnbkR23a7A9aLWXZUq7LqwL/dtBWfN9WZ7DlwvXV3r+WYXYgfwfW9wKoCFQljfcOS8B6ij+9aPzWtpwws4teN6hQOiAXXpjnIX0ic4NU8UKE9ZKxSoonDuAF1X/6FehL8CTBRQki7+qDe1HKlrluyikRH9qPoGjrTY/SdrggxpVTknFypF1vSz60rg5JbKyKIWAiclI2RRBZfdRmYFnAg4EWqoe4HzmnVABrURONesAzKoK4FTvra8Yy2QIV1IE8iR9tktWQ9kRGKU5dOGSDuZz1gXFFjSmbSoGpOfduvaqgSgKJRkhqmoT4vuazaWEP+DtUIBwHw2yrF3TENbliTUM5KBgoD5LKzXpdZaaA6A0OBgvgongKIBdP0WmK+GdUI88qxLB+XyvR9697t1ddBjtWCEBgHzIOesOehEHfQT64t8g3mQc9Yc9F5zUBwUeQezEZwAijroa1MwG8O5AyitdugQnTlmVm0e2Okqcy4PXWdciAG9Dea67YM7dcKrVV/yHqC+wewMJ4CiPZIvbU66gtk5rBPi0R4wi76uQ9/bgJs6KC13gNnbDRJ6nb6kJxQw+3zS3keDATQ53fkAs9ecc08OKmGepuVx682WN7NwKqBF9mu6k44mwLQFpwJKyx1g2sg59+SgNC0Dpk04AXRUuhwKzEHC+p4QLzko1wSNEMw2nUXRwKmA0rQMmHbC+k6IpycUMG3CCaBR6XsoMIOEdUJ8NDrro7MoWjhrkMoi0NEEmOHD+p4wL6GDnlDAtAcngJrQgyUwzYR1QrwJML10Fo3COXcc9Cu8pA2mSeesOajck/Nv2PGuPyyCadI5a5rAzSC6t/rCLMN5DDeDqADOES3ayHQMnM3yzVlGczJwsmDJaw6chHSz0lkv4MQ5We/Y4ZzDC5HKHJxWQwzOCZyE9DCaaoUEONkM4Z44JwLOHvNN+T6dsWEilknnJKSH0xw4CelmpRe9AE6ck9AeG5wU34lc9uC0FlJwTuAk37SlqVZMgJN8k9COc6IoTcIEnBpKuF4ScJp0TlyTCGYWTvJNO8qtVE6AE5l1T8I6As43NkOzjDFgNkVGnZOQbk+fgJOQblYWZrlwTmT2fbEAJ8V3NkX24GQMGOe07JzAaVd56HHh0HCOaTMkN1v4nI3rxl9B35/fgLOz7tyx/Jh/21T/T85xSneWy4bHXr+VyHaVnHNqyIh9DPi7wFgD86fcv5f6wbvGOeN0zphd81bd8tUL/bu/24rz6I0XykhdNOhMV8icM8bNkNwW5asD7/jQO1C4n1u5k0SJf2P8FIasqISEMzbnvNYQvmr6QLlLsjsW7o8nmqMS2o3DGcsYsLjliYOr0FDdWu7xV/pmXwCnUTgjGgOW8tBMoepF6qKSh8ZSdkourFvPNyX0fpZbHQpMPn6B7PAld9Udv2UFGxcOBadl59xbHvIlLTv9ntkuOxUpwWnROSXEyn0gS19u+QagW8lps183pH0g7wwEp8FrcDYuD3mE1GrZKRnntOSarctDHgG1WHaapwKnhXxT3PKsj/KQR0hNlZ1CVFhSdE4JmVIeWhvfJVsrOyUBZyjnrMpDi6E3PD1AaqHsVIwaTg0NIRogLrIBy0MeIS2zcGWnwU1l6K6koYu573YPRQio5Mghup0Gn/U6ysYp2fB8t1Ae8ghp1N1OqcJZlYfKbOSKvNvJHJxbz25pujzkEdIhyk63o4ZTofHxCY+mPOTZRX2WnTajhlPV57cx0ZaHPELqq+y0SgHOdU/uOYrykEdIJefuq+x0ESJV+vD4+Dj4wmm9U6BqUwYZXXlogPXuUnaS9S5CRKYgcLYEVDY8qxR24Z7We6JR60sMYAaFUxdspp/o03d+9Frdcgtmndd8ofnj1LoRBIVzB1JZtCJ7/i2SOOsaKL1BKutd/1pyq2t+ZWGD+b8AAwD5SxZe2adjXAAAAABJRU5ErkJggg==";

              container = jQuery("<div/>", {
                  "id": "plugin_container"
              });

              textBlock = jQuery("<div/>", {
                  "id": "text_block"
              }).text(text || "Click to add the game to bookmarks").appendTo(container);

              closeBtn = jQuery("<span/>", {
                  "id": "plugin_close",
                  click: function click() {
                      jQuery(this).parent().hide();
                  }
              }).text("x").appendTo(textBlock);

              link = jQuery("<a/>", {
                  "id": "add_to_favorite",
                  "href": window.location.href + "?bookmark"
              }).appendTo(container);

              img = jQuery("<img>", {
                  "src": linkImg || src,
                  "alt": document.title
              }).appendTo(link);

              jQuery("body").append(container);
          }

          ///<summary>
          ///detects browser and returns specific message 
          ///</summary>
          function getMessageForBrowser() {
              var sBrowser = void 0,
                  sUsrAg = navigator.userAgent;

              if (sUsrAg.indexOf("Firefox") > -1) {
                  sBrowser = "Click the star to Bookmark the game";
              } else {
                  sBrowser = "Drag the star to your Bookmarks to save the game";
              }
              return sBrowser;
          }

          ///<summary>
          ///returns bool
          ///</summary>
          function isMobile() {
              return window.screen.width < 800;
          }

          function privatInit(link) {
              if (!isjQEnabled()) {
                  console.warn("There is no jQ");
                  return;
              }
              if (isMobile()) {
                  console.warn("Mobile");
                  return;
              }

              var msg = getMessageForBrowser();
              createHtmlTemplate(link, msg);
              clickHandler();
          }

          var Favorite = function () {
              function Favorite() {
                  var settings = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : { link: null, timer: 30000 };

                  _classCallCheck(this, Favorite);

                  this.link = settings.link;
                  this.timer = settings.timer;
              }

              _createClass(Favorite, [{
                  key: "init",
                  value: function init() {
                      var _this = this;

                      setTimeout(function () {
                          privatInit(_this.link);
                      }, this.timer);
                  }
              }]);

              return Favorite;
          }();

          var obj = new Favorite({ link: null, timer: 30000 });
          obj.init();
      })(jQuery);
  });

/***/ }
/******/ ]);