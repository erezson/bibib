

var upload_image_button=false;
jQuery(document).ready(function($) {
jQuery('.upload_image_button').click(function() {
    upload_image_button = true;
    formfieldID=jQuery(this).prev().attr("id");
    formfield = jQuery("#"+formfieldID).attr('name');
    tb_show('', 'media-upload.php?type=image&amp;TB_iframe=true');
    if(upload_image_button==true){

        var oldFunc = window.send_to_editor;
        window.send_to_editor = function(html) {

            imgurl = jQuery("<div>" + html + "</div>").find('img').attr('src');
            jQuery("#"+formfieldID).val(imgurl);
            tb_remove();
            window.send_to_editor = oldFunc;
            jQuery(this).parent('tr').next().find('img').attr('src',imgurl);
            jQuery('#saveHero').trigger('click');
        }
    }
    upload_image_button=false;
});
});

/*
jQuery(document).ready(function($) {
    $('.upload_image_button').click(function() {
        tb_show('Upload a background', 'media-upload.php?type=image&TB_iframe=true', false);

        return false;
    });
    window.send_to_editor = function(html) {
        media_url = jQuery("<div>" + html + "</div>").find('img').attr('src');
        tb_remove();
        $('#bg_url').val(media_url);
        $('#previewBG').attr('src',media_url);
        $('#saveHero').trigger('click');
    }
});*/